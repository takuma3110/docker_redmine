-- schema
DELETE FROM mysql.user WHERE User='';
DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1');
-- DROP DATABASE test;
-- DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
CREATE SCHEMA IF NOT EXISTS `redmine` DEFAULT CHARACTER SET utf8 ;
GRANT ALL PRIVILEGES ON redmine.* TO redmine@'%' IDENTIFIED BY 'redmine' WITH GRANT OPTION;
FLUSH PRIVILEGES;